import 'package:flutter/material.dart';

enum AppTheme {
  lightTheme,
  darkTheme,
}

Map<AppTheme, dynamic> appThemeData = {
  AppTheme.lightTheme: ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.green,
  ),
  AppTheme.darkTheme: ThemeData(
    brightness: Brightness.dark,
    primaryColor: Colors.grey,
  ),
};
