import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:untitled/app_theme.dart';
import 'package:untitled/bloc/app_theme_bloc.dart';
import 'package:untitled/form_bloc/bloc/login_bloc_bloc.dart';
import 'package:untitled/validators.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (ctx) => AppThemeBloc(),
      child: BlocProvider(
        create: (ctx) => LoginBlocBloc(),
        child: BlocConsumer<AppThemeBloc, AppThemeState>(
          listener: (ctx, _) {},
          // bloc: BlocProvider.of<AppThemeBloc>(context)
          //   ..add(const OnInitialAppThemeEvent()),
          builder: (ctx, state) {
            return MaterialApp(
              title: 'Theme',
              theme: state.appTheme == AppTheme.lightTheme
                  ? appThemeData[AppTheme.lightTheme]
                  : appThemeData[AppTheme.darkTheme],
              home: HomeScreen(),
            );
          },
        ),
      ),
    );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Switch Theme'),
        actions: [
          BlocConsumer<AppThemeBloc, AppThemeState>(
              bloc: BlocProvider.of<AppThemeBloc>(context, listen: false)
                ..add(OnInitialAppThemeEvent()),
              listener: (cctx, data) {},
              builder: (ctx, state) {
                return Switch(
                    value: state.isDarkTheme,
                    onChanged: (val) async {
                      BlocProvider.of<AppThemeBloc>(context, listen: false)
                          .add(OnAppThemeChangeEvent(isDarkTheme: val));

                      SharedPreferences sharedPrefs =
                          await SharedPreferences.getInstance();
                      await sharedPrefs.setBool('isDarkTheme', val);
                    });
              }),
        ],
      ),
      body: BlocBuilder<LoginBlocBloc, LoginBlocState>(
          // bloc: BlocProvider.of<LoginBlocBloc>(context, listen: false)
          //   ..add(InitialEvent()),
          builder: (ctx, state) {
            return Form(
              key: state.key,
              child: Column(
                children: [
                  TextFormField(
                    initialValue: state.userName,
                    decoration: const InputDecoration(
                      icon: Icon(Icons.person),
                      labelText: 'User Name',
                      helperText: '@Jhon....',
                    ),
                    keyboardType: TextInputType.text,
                    validator: Validator.fullNameValidator,
                    onSaved: (value) {
                      BlocProvider.of<LoginBlocBloc>(context, listen: false)
                          .add(UserNameEvent(userName: value!));
                    },
                    textInputAction: TextInputAction.next,
                  ),
                  TextFormField(
                    initialValue: state.password,
                    validator: Validator.passwordValidator,
                    decoration: const InputDecoration(
                      icon: Icon(Icons.password),
                      labelText: 'Password',
                      helperText: '*********',
                    ),
                    keyboardType: TextInputType.visiblePassword,
                    onSaved: (value) {
                      BlocProvider.of<LoginBlocBloc>(context, listen: false)
                          .add(PasswordEvent(password: value!));
                    },
                    textInputAction: TextInputAction.next,
                  ),
                  TextButton(
                    onPressed: () {
                      BlocProvider.of<LoginBlocBloc>(context, listen: false)
                          .add(SubmitEvent());
                    },
                    child: const Text('Submit'),
                  )
                ],
              ),
            );
          }),
    );
  }
}
