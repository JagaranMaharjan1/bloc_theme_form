part of 'login_bloc_bloc.dart';

class LoginBlocState extends Equatable {
  final String userName;
  final String password;
  final GlobalKey<FormState> key = GlobalKey<FormState>();

  LoginBlocState({
    required this.userName,
    required this.password,
  });

  LoginBlocState copyWith({
    String? userName,
    String? password,
  }) {
    return LoginBlocState(
      userName: userName ?? this.userName,
      password: password ?? this.password,
    );
  }

  @override
  List<Object> get props => [
        key,
        userName,
        password,
      ];
}

class LoginBlocInitial extends LoginBlocState {
  LoginBlocInitial({required String? userName, required String? password})
      : super(userName: userName!, password: password!);
}
