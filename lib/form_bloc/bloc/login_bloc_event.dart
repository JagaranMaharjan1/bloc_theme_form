part of 'login_bloc_bloc.dart';

abstract class LoginBlocEvent extends Equatable {
  const LoginBlocEvent();

  @override
  List<Object> get props => [];
}

class UserNameEvent extends LoginBlocEvent {
  final String userName;

  const UserNameEvent({
    required this.userName,
  });
}

class PasswordEvent extends LoginBlocEvent {
  final String password;

  const PasswordEvent({
    required this.password,
  });
}

class InitialEvent extends LoginBlocEvent {}

class SubmitEvent extends LoginBlocEvent {}
