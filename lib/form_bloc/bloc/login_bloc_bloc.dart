import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'login_bloc_event.dart';

part 'login_bloc_state.dart';

class LoginBlocBloc extends Bloc<LoginBlocEvent, LoginBlocState> {
  LoginBlocBloc() : super(LoginBlocInitial(userName: '', password: '')) {
    on<LoginBlocEvent>((event, emit) {});
    on<UserNameEvent>((event, emit) {
      emit(state.copyWith(userName: event.userName));
    });
    on<PasswordEvent>((event, emit) {
      emit(state.copyWith(password: event.password));
    });
    on<InitialEvent>((event, emit) {
      emit(
        state.copyWith(
          userName: 'Jagaran Maharjan',
          password: 'Password123',
        ),
      );
    });
    on<SubmitEvent>((event, emit) async {
      if (state.key.currentState!.validate()) {
        state.key.currentState!.save();
        await Future.delayed(const Duration(milliseconds: 300));

        print(
            'State Has User Name : ${state.userName}, Password : ${state.password}');
        emit(state.copyWith(userName: '', password: ''));
      }
    });
  }
}