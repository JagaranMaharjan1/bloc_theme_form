class Validator {
  static String? fullNameValidator(String? value) {
    if (value!.isEmpty) {
      return 'Please enter the full name';
    }
    if (value.trim().length < 8) {
      return 'Please enter the full name';
    }
    return null;
  }


  static String? passwordValidator(String? value) {
    if (value!.isEmpty) {
      return 'Please enter the password';
    }
    if (value.trim().length < 8) {
      return 'Password must contain min.8 digits';
    }
    return null;
  }
}
