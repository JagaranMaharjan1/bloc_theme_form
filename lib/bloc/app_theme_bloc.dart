import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_theme.dart';

part 'app_theme_event.dart';

part 'app_theme_state.dart';

class AppThemeBloc extends Bloc<AppThemeEvent, AppThemeState> {
  AppThemeBloc()
      : super(const AppThemeState(
          appTheme: AppTheme.lightTheme,
          isDarkTheme: false,
        )) {
    on<OnAppThemeChangeEvent>(_onAppThemeChangeEvent);
    on<OnInitialAppThemeEvent>(_onInitialAppEventTheme);
  }

  void _onAppThemeChangeEvent(
      OnAppThemeChangeEvent event, Emitter<AppThemeState> emit) {
    emit(
      AppThemeState(
        appTheme: event.isDarkTheme ? AppTheme.darkTheme : AppTheme.lightTheme,
        isDarkTheme: event.isDarkTheme,
      ),
    );
  }

  void _onInitialAppEventTheme(
      OnInitialAppThemeEvent event, Emitter<AppThemeState> emit) async {
    SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
    if (sharedPrefs.containsKey('isDarkTheme')) {
      bool? isDarkTheme = sharedPrefs.getBool('isDarkTheme');
      emit(
        AppThemeState(
          appTheme: isDarkTheme! ? AppTheme.darkTheme : AppTheme.lightTheme,
          isDarkTheme: isDarkTheme,
        ),
      );
    } else {
      emit(const AppThemeState(
        appTheme: AppTheme.lightTheme,
        isDarkTheme: false,
      ));
    }
  }
}
