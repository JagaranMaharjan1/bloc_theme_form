part of 'app_theme_bloc.dart';

@immutable
abstract class AppThemeEvent {}

class OnAppThemeChangeEvent extends AppThemeEvent {
  final bool isDarkTheme;

  OnAppThemeChangeEvent({this.isDarkTheme = false});

  List<Object> get props => [isDarkTheme];
}

class OnInitialAppThemeEvent extends AppThemeEvent {
  OnInitialAppThemeEvent();
}
