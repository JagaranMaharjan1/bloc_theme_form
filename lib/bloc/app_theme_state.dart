part of 'app_theme_bloc.dart';

class AppThemeState extends Equatable {
  final AppTheme appTheme;
  final bool isDarkTheme;

  const AppThemeState({
    required this.appTheme,
    required this.isDarkTheme,
  });

  @override
  List<Object> get props => [appTheme];
}
